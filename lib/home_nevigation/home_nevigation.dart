import 'package:flutter/material.dart';
import 'package:levelup/feed/feed.dart';
import 'package:levelup/pages/add_item_screen/add_item_screen.dart';
import 'package:levelup/pages/profile_screen/profile_screen.dart';
/*
according to https://willowtreeapps.com/ideas/how-to-use-flutter-to-build-an-app-with-bottom-navigation
*/

class HomeNevigation extends StatefulWidget {
  HomeNevigation({Key key}) : super(key: key);

  @override
  _HomeNevigationState createState() => _HomeNevigationState();
}

class _HomeNevigationState extends State<HomeNevigation> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Feed(),
    AddItemScreen(),
    ProfileScreen(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: _children[_currentIndex], // new
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Color(0xFFFF5252),
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.add_box),
            title: Text('add'),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text('Me'))
        ],
      ),
    );
  }
}

class PlaceholderWidget extends StatelessWidget {
  final Color color;

  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(color: color);
  }
}
