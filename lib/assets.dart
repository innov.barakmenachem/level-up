import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

final Widget svg_logo =
    SvgPicture.asset('assets/svg/logo.svg', semanticsLabel: 'LevelUp Logo');



final Widget svg_like_color =
    SvgPicture.asset('assets/svg/like_color.svg', semanticsLabel: 'LevelUp like color', );


final Widget svg_recording_studio =
    SvgPicture.asset('assets/svg/recording_studio.svg', semanticsLabel: 'LevelUp recording studio', width: 300,);


