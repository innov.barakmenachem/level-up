import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:levelup/firebase_api/firebase_easy_api.dart';
import 'package:levelup/modules/user.dart';

/*
sign in with google according to https://medium.com/flutter-community/flutter-implementing-google-sign-in-71888bca24ed
*/

// Add these three variables to store the info
// retrieved from the FirebaseUser
User currentUserModule;
final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<void> setUserData(FirebaseUser user) async {
  assert(user.uid != null);
  assert(user.email != null);
  assert(user.displayName != null);
  assert(user.photoUrl != null);
  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);


  currentUserModule = User.fromFirebase(user);
  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);
  
  FirebaseEasyApi database = new FirebaseEasyApi();
  await database.postUserMetadata(currentUserModule);
}

Future<FirebaseUser> getCurrentUserIfExists() async {
  FirebaseUser _user = await FirebaseAuth.instance.currentUser();

  if (_user != null && !_user.isAnonymous) {
    print("User: ${_user.displayName ?? "None"}");
    await setUserData(_user);
    return _user;
  }
  return null;
}

Future<String> signInWithGoogle() async {
  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  final AuthResult authResult = await _auth.signInWithCredential(credential);
  final FirebaseUser user = authResult.user;
  setUserData(user);
  return 'signInWithGoogle succeeded: $user';
}

void signOutGoogle() async {
  await FirebaseAuth.instance.signOut();
  await googleSignIn.signOut();
  print("User Sign Out");
}
