import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:levelup/feed/post.dart';
import 'package:levelup/firebase_api/firebase_easy_api.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';
import 'package:levelup/pages/login_and_register_page/login_and_register_page.dart';

class FollowSection extends StatefulWidget {
  FollowSection({Key key}) : super(key: key);

  @override
  _FollowSectionState createState() => _FollowSectionState();
}

class _FollowSectionState extends State<FollowSection> {
  FirebaseEasyApi database = FirebaseEasyApi();
  @override
  Widget build(BuildContext context) {
    bool _isButtonDisabled = false;
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          OutlineButton(
            splashColor: Colors.grey,
            onPressed: _isButtonDisabled ? null : () {},
            borderSide: BorderSide(color: Colors.grey),
            child: Text(
              ' Follow ',
              style: TextStyle(
                fontSize: 20,
                color: Colors.grey,
              ),
            ),
          ),
          Text(
            ' 1.2M ',
            style: TextStyle(
              fontSize: 20,
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30),
          CircleAvatar(
            backgroundImage: NetworkImage(
              currentUserModule.imageUrl,
            ),
            radius: 50,
            backgroundColor: Colors.transparent,
          ),
          SizedBox(height: 10),
          Text(
            currentUserModule.email,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: Colors.black54),
          ),
          SizedBox(height: 10),
          FollowSection()
        ],
      ),
    );
  }
}

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FeedSilverList();
  }
}

class FeedSilverList extends StatefulWidget {
  FeedSilverList({Key key}) : super(key: key);

  @override
  _FeedSilverListState createState() => _FeedSilverListState();
}

class _FeedSilverListState extends State<FeedSilverList> {
  FirebaseEasyApi database = FirebaseEasyApi();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          ///First sliver is the App Bar
          SliverAppBar(
            ///Properties of app bar
            backgroundColor: Colors.white,
            floating: false,
            pinned: true,
            expandedHeight: 250.0,
            automaticallyImplyLeading: false,

            ///Properties of the App Bar when it is expanded
            title: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    currentUserModule.name,
                    style:
                        TextStyle(color: Colors.grey),
                  ),
                  LogOutTitleBotton()
                ],
              ),
            ),
            flexibleSpace: FlexibleSpaceBar(
              background: Column(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  ProfileInfo()
                ],
              ),
            ),
          ),
          StreamBuilder(
            stream: database.getTextPostsStream(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return SliverPadding(
                  padding: const EdgeInsets.all(20.0),
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        DocumentSnapshot ds = snapshot.data.documents[index];
                        return new Row(
                          textDirection: TextDirection.ltr,
                          children: <Widget>[
                            Expanded(
                                child: TextPostCard(text: ds.data['text'])),
                          ],
                        );
                      },
                      childCount: snapshot.data.documents.length,
                    ),
                  ),
                );
              } else {
                return SliverFillRemaining(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            },
          )
        ],
      ),
    );
  }
}

class LogOutTitleBotton extends StatelessWidget {
  const LogOutTitleBotton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: () {
        signOutGoogle();

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) {
          return LoginPage();
        }), ModalRoute.withName('/'));
      },
      child: new Text(
        "Log Out",
        style: TextStyle(fontSize: 20, color: Colors.grey),
      ),
    );
  }
}
