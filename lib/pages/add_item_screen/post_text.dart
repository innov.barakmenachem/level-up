import 'package:flutter/material.dart';
import 'package:levelup/firebase_api/firebase_easy_api.dart';

class PostText extends StatefulWidget {
  @override
  _PostTextState createState() => _PostTextState();
}

class _PostTextState extends State<PostText> {
  final _formKey = GlobalKey<FormState>();
  final _textController = TextEditingController();
  FirebaseEasyApi database = new FirebaseEasyApi();
  bool _isButtonDisabled = false;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        child: Column(
          children: <Widget>[
            OutlineButton(
              splashColor: Colors.grey,
              onPressed: _isButtonDisabled
                  ? null
                  : () {
                      // Validate returns true if the form is valid, otherwise false.
                      if (_formKey.currentState.validate()) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.
                        setState(() {
                          _isButtonDisabled = true;
                        });

                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("Posting your inspiration...")));
                        String enteredText = _textController.text;
                        database.postText(enteredText).then((onValue) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text("Posting Done!")));
                          setState(() {
                            _isButtonDisabled = false;
                          });
                        });
                      }
                    },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40),
              ),
              borderSide: BorderSide(color: Colors.grey),
              child: Text(
                'Post',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
            ),
            SizedBox(height: 20),
            TextFormField(
              controller: _textController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter your inspiration text';
                }
                return null;
              },
              keyboardType: TextInputType.multiline,
              maxLines: 20,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 5.0)),
                  hintText: 'What\'s in your minde?'),
            ),
          ],
        ),
      ),
    );
  }
}
