import 'package:flutter/material.dart';
import 'package:levelup/pages/add_item_screen/post_text.dart';

class AddItemScreen extends StatefulWidget {
  AddItemScreen({Key key}) : super(key: key);

  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 20),
              Text(
                "Post your inspiration",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Text", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                  Text(" | ", style: TextStyle(fontSize: 20)),
                  Text("Melody", style: TextStyle(fontSize: 20)),
                  Text(" | ", style: TextStyle(fontSize: 20)),
                  Text("Video", style: TextStyle(fontSize: 20)),
                ],
              ),
              SizedBox(height: 20),
              PostText(),
            ],
          ),
        ),
      ),
    );
  }
}
