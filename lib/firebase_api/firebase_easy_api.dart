import 'dart:typed_data';
import 'package:levelup/feed/feed.dart';
import 'package:levelup/modules/post.dart';
import 'package:levelup/modules/user.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//imports to upload files:
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart'; // For File Upload To Firestore
import 'package:path/path.dart' as Path;
import 'package:random_string/random_string.dart';
import 'package:synchronized/synchronized.dart';

class Record {
  final idLen = 30;
  String _recordCloudstoreID = "";

  Record() {
    _recordCloudstoreID = randomAlphaNumeric(
        idLen); // random sequence of 10 alpha numeric i.e. aRztC1y32B
  }

  String getRecordCloudstoreID() {
    return _recordCloudstoreID;
  }
}

class FirebaseEasyApi {
  final databaseReference = Firestore.instance;
  CollectionReference userTextsPostsCollection;
  CollectionReference usersCollection;

  bool isFirstDocument = false;

  FirebaseEasyApi() {
    usersCollection = databaseReference.collection("users");
    userTextsPostsCollection = createTextPostCollection(currentUserModule.uid);
  }

  /*
   * userID - the id of the user that this is its own feed.
   * typeID - the type of the post we want to represend in the feed - can be: "texts", "videos" or "records"
   */
  CollectionReference createUserFeedCollection(String userID, String typeID) {
    return usersCollection
        .document(userID)
        .collection("feed")
        .document("posts")
        .collection(typeID);
  }

  CollectionReference createTextPostCollection(String userID) {
    return usersCollection
        .document(userID)
        .collection("all-posts")
        .document("texts")
        .collection("posts");
  }

  Future<void> postUserMetadata(User user) async {
    return usersCollection.document(user.uid).setData({
      'name': user.name,
      'email': user.email,
      'imageUrl': user.imageUrl,
      'uid': user.uid,
      'placeholder_field_to_able_get_all_users_id': null,
    });
  }

  /*--------------------------
   * Post Data
   * references:
   * 1. https://www.c-sharpcorner.com/article/upload-image-file-to-firebase-storage-using-flutter/
   * 
   * -------------------------
   */
  Future<void> postText(String text) async {
    return Firestore.instance.runTransaction((Transaction transactionHandler) {
      userTextsPostsCollection.add({
        'createdAt': new DateTime.now().millisecondsSinceEpoch.toString(),
        'text': text
      }).then((DocumentReference documentReference) {
        broadcastToAllFeeds(
            currentUserModule.uid, documentReference.documentID);
      });
    });
  }

  /**
   * posterUid - the id of the user that post the text
   * documentID - the id of the text that the user post.
   */
  Future<void> broadcastToAllFeeds(String posterUid, String documentID) async {
    //todo: move this functionallity to done by the server.(like firebase lambda).

    var len = await usersCollection
        .where('placeholder_field_to_able_get_all_users_id', isNull: true)
        .getDocuments()
        .then((QuerySnapshot query) {
      query.documents.forEach((doc) async {
        String uid = doc.documentID;
        if (uid != currentUserModule.uid) {
          await createUserFeedCollection(uid, "texts").add({
            "poster_uid": posterUid,
            "text_post_id": documentID,
          });
        } else {
          //post to his profile feed.
        }
      });
    });
  }

  Stream<QuerySnapshot> getTextPostsStream() {
    return userTextsPostsCollection
        .orderBy("createdAt", descending: true)
        .snapshots();
  }

  Future<void> postAudio(
      String recordFilePath, String uid, TextPost textPost) async {
    Record record = new Record();

    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child("Users/" + uid + '/Records/' + record.getRecordCloudstoreID());

    File recordFile = File(recordFilePath);
    StorageUploadTask uploadTask = storageReference.putFile(recordFile);
    await uploadTask.onComplete;

    print('File Uploaded');
    /*
    In order to get a download link of the record, I can add this lines:
    storageReference.getDownloadURL().then((fileURL) {
      print('FileURL:' + fileURL);
    });
    */

    //todo - update the feed and user and post...
    await updateCoverToTextPost(record, textPost);
  }

  Future<void> updateCoverToTextPost(Record record, TextPost textPost) {
    return userTextsPostsCollection
        .document(currentTextPost.firestoreID)
        .collection('covers')
        .document(record.getRecordCloudstoreID())
        .setData({});
  }

  //listeners
  /*
   * uid - is the uid of the user that want to see new posts of other uids. 
   */
  Stream listenToPosts(String uid) {
    return createUserFeedCollection(uid, "texts").snapshots();
  }

  Future<DocumentSnapshot> getUserDocument(String documentID) async {
    return await usersCollection.document(documentID).get();
  }

  Future<DocumentSnapshot> getTextPostDocument(
      String documentID, String uid) async {
        
    return await createTextPostCollection(uid).document(documentID).get();
  }
}
