import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class CoverBrowser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Icon(Icons.arrow_left),
          SizedBox(
            height: 100,
            width: 100,
            child: new FlareActor("assets/flare/dj.flr",
              alignment: Alignment.center,
              fit: BoxFit.contain,
              animation: "start"),
          ),
          
          Icon(Icons.arrow_right),
        ],
      ),
    ));
  }
}
