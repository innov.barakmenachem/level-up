import 'package:flutter/material.dart';

class CoverDescription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Best Cover:@firstjonny',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10),
          Text('Video title and some other stuff'),
          SizedBox(height: 5),
          Row(children: [
            Icon(Icons.music_note, size: 15.0),
            Text('Artist name - Album name - song',
                style: TextStyle(fontSize: 12.0))
          ]),
        ],
      ),
    );
  }
}
