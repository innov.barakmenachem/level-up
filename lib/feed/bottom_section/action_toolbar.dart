import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';
import 'package:levelup/feed/feed.dart';
import 'package:levelup/feed/feed_post.dart';
import 'package:levelup/feed/record_melody/my_record_player.dart';
import 'package:levelup/feed/record_melody/post_cover.dart';
import 'package:levelup/feed/record_melody/record_melody_bloc.dart';
import 'package:levelup/feed/record_melody/record_melody_page.dart';
import 'package:levelup/feed/record_melody/record_player.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';

class ActionsToolbar extends StatefulWidget {
  ActionsToolbar({Key key}) : super(key: key);

  @override
  _ActionsToolbarState createState() => _ActionsToolbarState();
}

class _ActionsToolbarState extends State<ActionsToolbar> {
  RecordMelodyBloc recordMelodyBloc = new RecordMelodyBloc();
  final FlareControls controls = FlareControls();
  bool isLike = false;
  void _playUnlikeAnimation() {
    // Use the controls to trigger an animation.
    controls.play("unlike");
    isLike = false;
  }

  void _playLikeAnimation() {
    // Use the controls to trigger an animation.
    controls.play("like");
    isLike = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(right: 10.0, bottom: 20.0),
        child: Column(
            //in order to center the animation of the levelup with other action bottons
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (isLike) {
                        _playUnlikeAnimation();
                      } else {
                        _playLikeAnimation();
                      }
                    },
                    child: SizedBox(
                      height: 70,
                      width: 70,
                      child: FlareActor("assets/flare/levelup.flr",
                          animation: "unlike",
                          fit: BoxFit.contain,
                          alignment: Alignment.center,
                          controller: controls),
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.local_activity,
                          size: 35.0, color: Colors.grey[300]),
                      onPressed: () {}),
                  Text("3.2M", style: TextStyle(fontSize: 12.0)),
                  IconButton(
                      icon: Icon(Icons.chat_bubble,
                          size: 35.0, color: Colors.grey[300]),
                      onPressed: () {}),
                  Text("16.4k", style: TextStyle(fontSize: 12.0)),
                  IconButton(
                      icon: Icon(Icons.replay,
                          size: 35.0, color: Colors.grey[300]),
                      onPressed: () {}),
                  Text("Share", style: TextStyle(fontSize: 12.0)),
                  IconButton(
                      icon: Icon(Icons.record_voice_over,
                          size: 35.0, color: Colors.grey[300]),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PostCoverPage()),
                        );
                      }),
                  Text("cover", style: TextStyle(fontSize: 12.0)),
                ],
              ),
            ]));
  }
}
