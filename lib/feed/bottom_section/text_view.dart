import 'package:flutter/material.dart';
import 'package:levelup/feed/feed.dart';
import 'package:levelup/feed/feed_post.dart';
import 'package:levelup/feed/record_melody/index.dart';
import 'package:levelup/feed/record_melody/my_record_player.dart';

class TextView extends StatefulWidget {
  TextView({Key key, @required this.text}) : super(key: key);
  final String text;

  @override
  _TextViewState createState() => _TextViewState();
}

class _TextViewState extends State<TextView> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Align(
        alignment: Alignment.topLeft,
        child: new SingleChildScrollView(
          child: Padding(
            padding: EdgeInsetsDirectional.only(start: 10),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(widget.text),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
