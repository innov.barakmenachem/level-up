import 'package:flutter/material.dart';
import 'package:levelup/feed/bottom_section/action_toolbar.dart';
import 'package:levelup/feed/bottom_section/text_view.dart';
import 'package:levelup/feed/middle_section/cover_browser.dart';
import 'package:levelup/feed/middle_section/cover_descriptiom.dart';
import 'package:levelup/feed/record_melody/my_record_player.dart';
import 'package:levelup/modules/post.dart';
import 'package:levelup/feed/feed.dart';
import 'package:levelup/pages/add_item_screen/post_text.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';
/*
Feed created according to:
https://medium.com/filledstacks/breaking-down-tiktoks-ui-using-flutter-8489fe4ad944
https://medium.com/filledstacks/building-tiktoks-ui-in-flutter-part-2-build-the-small-parts-42fb2089d605
*/

bool isCoverOpen = false;

class FeedPost extends StatefulWidget {
  FeedPost({Key key, @required this.textPostFuture}) : super(key: key);
  final Future<TextPost> textPostFuture;

  @override
  _FeedPostState createState() => _FeedPostState();
}

class _FeedPostState extends State<FeedPost> {
  TextPost textPost;
  bool isLoaded = false;

  @override
  Widget build(BuildContext context) {
    widget.textPostFuture.then((loadedTextPost) {
      setState(() {
        textPost = loadedTextPost;
        isLoaded = true;
      });
    });
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 30),
        child: Column(
          children: <Widget>[
            // Top section
            TopSection(
              textTitle: isLoaded ? textPost.textTitle : "loading title...",
              textCreatorName:
                  isLoaded ? textPost.textCreatorUser.name : "loading name...",
            ),

            // Middle expanded
            middleSection,

            //Bottom section
            BottomSection(
              text: isLoaded ? textPost.text : "loading song...",
            ),
          ],
        ),
      ),
    );
  }
}

class TopSection extends StatelessWidget {
  TopSection(
      {Key key, @required this.textTitle, @required this.textCreatorName})
      : super(key: key);
  final String textTitle;
  final String textCreatorName;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, top: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            textTitle + " / @" + textCreatorName,
            style: TextStyle(fontSize: 20),
          ),
        ],
      ),
    );
  }
}

Widget get middleSection => Container(
      height: 120.0,
      child: Stack(
        children: <Widget>[
          CoverDescription(),
          Align(
            alignment: Alignment.topRight,
            child: Container(
              child: CoverBrowser(),
              width: 150,
            ),
          )
        ],
      ),
    );

class BottomSection extends StatelessWidget {
  BottomSection({Key key, @required this.text}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
          TextView(text: text),
          ActionsToolbar(),
        ]));
  }
}
