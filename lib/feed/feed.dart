import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:levelup/feed/feed_post.dart';
import 'package:levelup/firebase_api/firebase_easy_api.dart';
import 'package:levelup/modules/post.dart';
import 'package:levelup/modules/user.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';

TextPost currentTextPost = TextPost(
    textCreatorUser: currentUserModule, textTitle: "Hello", text: "song");

class Feed extends StatefulWidget {
  Feed({Key key}) : super(key: key);

  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {
  FirebaseEasyApi database = new FirebaseEasyApi();
  bool _isTextPostLoaded = false;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: database.listenToPosts(currentUserModule.uid),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return new Text('Loading...');
        return new ListView(
          children: snapshot.data.documents.map((DocumentSnapshot document) {
            Future<TextPost> textPostFuture = _loadTextPost(document);
            return !_isTextPostLoaded
                ? Center(child: CircularProgressIndicator())
                : Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: new FeedPost(textPostFuture: textPostFuture),
                  );
          }).toList(),
        );
      },
    );
  }

  Future<TextPost> _loadTextPost(
      DocumentSnapshot document) async {
    User textPostCreatorUser;
    await database
        .getUserDocument(document.data['poster_uid'])
        .then((DocumentSnapshot userDocument) {
      textPostCreatorUser = User.fromFirestore(userDocument);
    });

    TextPost textpost;
    await database
        .getTextPostDocument(
            document.data['text_post_id'], textPostCreatorUser.uid)
        .then((DocumentSnapshot textPostDocument) {
      textpost = TextPost.fromFirestore(textPostDocument, textPostCreatorUser);
    });

    if (!mounted) {
      return null;
    }
    setState(() {
      _isTextPostLoaded = true;
    });
    return textpost;
  }

}
