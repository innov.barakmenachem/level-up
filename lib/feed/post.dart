
import 'package:flutter/material.dart';

class TextPostCard extends StatefulWidget {
  TextPostCard({Key key, @required this.text}) : super(key: key);
  final String text;
  @override
  _TextPostCardState createState() => _TextPostCardState();
}

class _TextPostCardState extends State<TextPostCard> {
  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Column(
        children: <Widget>[
          new Image.network(
              'https://cdn.stocksnap.io/img-thumbs/280h/MWSVZ9XPTV.jpg'),
          new Padding(
              padding: new EdgeInsets.all(7.0),
              child: Column(children: [
                Text(
                  "Song:",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(widget.text),
                new Row(
                  children: <Widget>[
                    new Padding(
                      padding: new EdgeInsets.all(7.0),
                      child: new Icon(Icons.thumb_up),
                    ),
                    new Padding(
                      padding: new EdgeInsets.all(7.0),
                      child: new Text(
                        'Like',
                        style: new TextStyle(fontSize: 18.0),
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.all(7.0),
                      child: new Icon(Icons.comment),
                    ),
                    new Padding(
                      padding: new EdgeInsets.all(7.0),
                      child: new Text('Comments',
                          style: new TextStyle(fontSize: 18.0)),
                    )
                  ],
                )
              ]))
        ],
      ),
    );
  }
}
