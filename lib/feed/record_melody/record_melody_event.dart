import 'dart:async';
import 'dart:developer' as developer;

import 'package:levelup/feed/record_melody/index.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RecordMelodyEvent {
  Future<RecordMelodyState> applyAsync(
      {RecordMelodyState currentState, RecordMelodyBloc bloc});
  final RecordMelodyRepository _recordMelodyRepository = RecordMelodyRepository();
}

class UnRecordMelodyEvent extends RecordMelodyEvent {
  @override
  Future<RecordMelodyState> applyAsync({RecordMelodyState currentState, RecordMelodyBloc bloc}) async {
    return UnRecordMelodyState(0);
  }
}

class LoadRecordMelodyEvent extends RecordMelodyEvent {
  final bool isError;
  @override
  String toString() => 'LoadRecordMelodyEvent';

  LoadRecordMelodyEvent(this.isError);

  @override
  Future<RecordMelodyState> applyAsync(
      {RecordMelodyState currentState, RecordMelodyBloc bloc}) async {
    try {
      if (currentState is InRecordMelodyState) {
        return currentState.getNewVersion();
      }
      await Future.delayed(Duration(seconds: 2));
      this._recordMelodyRepository.test(this.isError);
      return InRecordMelodyState(0, "Hello world");
    } catch (_, stackTrace) {
      developer.log('$_', name: 'LoadRecordMelodyEvent', error: _, stackTrace: stackTrace);
      return ErrorRecordMelodyState(0, _?.toString());
    }
  }
}
