import 'package:equatable/equatable.dart';

abstract class RecordMelodyState extends Equatable {
  /// notify change state without deep clone state
  final int version;
  
  final List propss;
  RecordMelodyState(this.version,[this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  RecordMelodyState getStateCopy();

  RecordMelodyState getNewVersion();

  @override
  List<Object> get props => (propss);
}

//main state:
class MainRecordMelodyState extends RecordMelodyState {

  MainRecordMelodyState(int version) : super(version);

  @override
  String toString() => 'MainRecordMelodyState';

  @override
  MainRecordMelodyState getStateCopy() {
    return MainRecordMelodyState(0);
  }

  @override
  MainRecordMelodyState getNewVersion() {
    return MainRecordMelodyState(version+1);
  }
}


/// UnInitialized
class UnRecordMelodyState extends RecordMelodyState {

  UnRecordMelodyState(int version) : super(version);

  @override
  String toString() => 'UnRecordMelodyState';

  @override
  UnRecordMelodyState getStateCopy() {
    return UnRecordMelodyState(0);
  }

  @override
  UnRecordMelodyState getNewVersion() {
    return UnRecordMelodyState(version+1);
  }
}

/// Initialized
class InRecordMelodyState extends RecordMelodyState {
  final String hello;

  InRecordMelodyState(int version, this.hello) : super(version, [hello]);

  @override
  String toString() => 'InRecordMelodyState $hello';

  @override
  InRecordMelodyState getStateCopy() {
    return InRecordMelodyState(this.version, this.hello);
  }

  @override
  InRecordMelodyState getNewVersion() {
    return InRecordMelodyState(version+1, this.hello);
  }
}

class ErrorRecordMelodyState extends RecordMelodyState {
  final String errorMessage;

  ErrorRecordMelodyState(int version, this.errorMessage): super(version, [errorMessage]);
  
  @override
  String toString() => 'ErrorRecordMelodyState';

  @override
  ErrorRecordMelodyState getStateCopy() {
    return ErrorRecordMelodyState(this.version, this.errorMessage);
  }

  @override
  ErrorRecordMelodyState getNewVersion() {
    return ErrorRecordMelodyState(version+1, this.errorMessage);
  }
}
