import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:levelup/assets.dart';
import 'package:levelup/feed/feed_post.dart';
import 'package:levelup/feed/record_melody/record_melody_bloc.dart';

class MyRecordPlayer extends StatefulWidget {
  MyRecordPlayer({Key key, @required RecordMelodyBloc recordMelodyBloc})
      : _recordMelodyBloc = recordMelodyBloc,
        super(key: key);
  final RecordMelodyBloc _recordMelodyBloc;
  @override
  _MyRecordPlayerState createState() => _MyRecordPlayerState();
}

class _MyRecordPlayerState extends State<MyRecordPlayer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      color: Colors.indigo[100],
      child: Stack(
        children: <Widget>[
          Align(alignment: Alignment.topRight, child: svg_recording_studio),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Center(
                    child: Text("00:32", style: TextStyle(fontSize: 18)),
                  ),
                ),
              ),
              Container(
                height: 50,
                width: 130,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.replay),
                    Icon(Icons.pause_circle_filled),
                    Icon(Icons.mic),
                  ],
                ),
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.linear_scale),
                    Icon(
                      Icons.play_arrow,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
