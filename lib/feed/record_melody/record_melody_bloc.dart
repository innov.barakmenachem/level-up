import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:levelup/feed/record_melody/index.dart';
/*
 * 
 * https://bloclibrary.dev/#/fluttertimertutorial
 */
class RecordMelodyBloc extends Bloc<RecordMelodyEvent, RecordMelodyState> {
  // todo: check singleton for logic in project
  static final RecordMelodyBloc _recordMelodyBlocSingleton = RecordMelodyBloc._internal();
  factory RecordMelodyBloc() {
    return _recordMelodyBlocSingleton;
  }
  RecordMelodyBloc._internal();
  
  @override
  Future<void> close() async{
    // dispose objects
    super.close();
  }

  RecordMelodyState get initialState => MainRecordMelodyState(0);

  @override
  Stream<RecordMelodyState> mapEventToState(
    RecordMelodyEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'RecordMelodyBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }
}
