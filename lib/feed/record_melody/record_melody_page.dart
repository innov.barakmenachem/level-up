import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:levelup/feed/bottom_section/text_view.dart';
import 'package:levelup/feed/feed_post.dart';
import 'package:levelup/feed/record_melody/index.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:levelup/feed/record_melody/my_record_player.dart';
import 'package:levelup/feed/record_melody/record_player.dart';
import 'package:levelup/modules/post.dart';

class RecordMelodyPage extends StatefulWidget {
  RecordMelodyPage(
      {Key key,
      @required RecordMelodyBloc recordMelodyBloc,
      @required TextPost textPost})
      : _recordMelodyBloc = recordMelodyBloc,
        _textPost = textPost,
        super(key: key);
  final RecordMelodyBloc _recordMelodyBloc;
  final TextPost _textPost;
  @override
  _RecordMelodyPageState createState() => _RecordMelodyPageState();
}

class _RecordMelodyPageState extends State<RecordMelodyPage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecordMelodyBloc, RecordMelodyState>(
        bloc: widget._recordMelodyBloc,
        builder: (
          BuildContext context,
          RecordMelodyState currentState,
        ) {
          if (currentState is MainRecordMelodyState) {
            return Scaffold(body:
            Column(
              children: <Widget>[
                TopSection(textTitle: "tmp in bloc",textCreatorName: "tmp in bloc",),
                
                TextView(text:"tmp in bloc"),
                
                MyRecordPlayer(
                  recordMelodyBloc: widget._recordMelodyBloc,
                ),
              ],
            ));
          }
          if (currentState is UnRecordMelodyState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (currentState is ErrorRecordMelodyState) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(currentState.errorMessage ?? 'Error'),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: RaisedButton(
                      color: Colors.blue,
                      child: Text('reload'),
                      onPressed: () => {} //this._load(),
                      ),
                ),
              ],
            ));
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Flutter files: done'),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: RaisedButton(
                      color: Colors.red,
                      child: Text('throw error'),
                      onPressed: () => {} //this._load(true),
                      ),
                ),
              ],
            ),
          );
        });
  }
}
