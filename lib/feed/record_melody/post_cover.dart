import 'package:flutter/material.dart';
import 'package:levelup/feed/bottom_section/text_view.dart';
import 'package:levelup/feed/feed.dart';
import 'package:levelup/feed/feed_post.dart';
import 'package:levelup/feed/record_melody/record_player.dart';
import 'package:levelup/firebase_api/firebase_easy_api.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';

class PostCoverPage extends StatefulWidget {
  PostCoverPage({Key key}) : super(key: key);

  @override
  _PostCoverPageState createState() => _PostCoverPageState();
}

class _PostCoverPageState extends State<PostCoverPage> {
  RecordController _recordController = new RecordController();
  bool _isButtonDisabled = false;
  FirebaseEasyApi database = new FirebaseEasyApi();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          TopSection(textCreatorName: "tmp",textTitle: "tmptmpt",),
          TextView(text: "tmp",),
          OutlineButton(
            splashColor: Colors.grey,
            onPressed: _isButtonDisabled
                ? null
                : () {
                    // Validate returns true if the form is valid, otherwise false.
                    if (_recordController.hasData()) {
                      // If the form is valid, display a snackbar. In the real world,
                      // you'd often call a server or save the information in a database.
                      setState(() {
                        _isButtonDisabled = true;
                      });

                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text("Posting your melody inspiration...")));
                      database
                          .postAudio(
                        _recordController.getData(),
                        currentUserModule.uid,
                        currentTextPost,
                      )
                          .then((onValue) {
                        _scaffoldKey.currentState.showSnackBar(
                            SnackBar(content: Text("Posting Done!")));
                        setState(() {
                          _isButtonDisabled = false;
                        });
                      });
                    }
                  },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40),
            ),
            borderSide: BorderSide(color: Colors.grey),
            child: Text(
              'Post',
              style: TextStyle(
                fontSize: 20,
                color: Colors.grey,
              ),
            ),
          ),
          RecordPlayer(
            recordController: _recordController,
          ),
        ],
      ),
    );
  }
}
