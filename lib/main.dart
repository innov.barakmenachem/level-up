import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:levelup/pages/login_and_register_page/login_and_register_page.dart';
import 'package:levelup/home_nevigation/home_nevigation.dart';
import 'package:levelup/pages/login_and_register_page/sign_in.dart';
import 'package:levelup/pages/waiting_login_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget currentPage = new WaitingLoginPage();
  bool islogin = false;
  @override
  void initState() {
    super.initState();
    _loadlogin();
  }

  Future<Null> _loadlogin() async{
    const timeOut = const Duration(
        milliseconds: 1000); //just for ux reseons to see the waiting page
    new Timer(timeOut, () {
      getCurrentUserIfExists().then((arguser) {
        setState(() {
          currentPage = arguser != null ? HomeNevigation() : LoginPage();
        });
      });
    });
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.light(),/*ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          backgroundColor: Colors.white,
          appBarTheme: AppBarTheme(color: Colors.black),
          scaffoldBackgroundColor: Colors.white),*/
      home: currentPage,
    );
  }
}
