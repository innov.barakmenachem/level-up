import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class User {
  final String name;
  final String email;
  final String imageUrl;
  final String uid;

  const User({
    this.name,
    this.email,
    this.imageUrl,
    this.uid,
  });

  factory User.fromFirebase(FirebaseUser user) {
    return new User(
      uid: user.uid,
      name: user.displayName,
      email: user.email,
      imageUrl: user.photoUrl,
    );
  }


  factory User.fromFirestore(DocumentSnapshot document) {
    return new User(
      uid: document.data['uid'],
      name: document.data['name'],
      email: document.data['email'],
      imageUrl: document.data['imageUrl'],
    );
  }
}
