import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:levelup/modules/user.dart';

class TextPost {
  User textCreatorUser = new User(
      email: "loading", imageUrl: "loading", uid: "lad", name: "oading name");
  String textTitle = "laoding title";
  String text = "loading text";
  String firestoreID = "ladoign";

  TextPost({this.textCreatorUser, this.textTitle, this.text, this.firestoreID});

  factory TextPost.fromFirestore(DocumentSnapshot document, User creator) {
    return new TextPost(
        firestoreID: document.documentID,
        text: document.data['text'],
        textTitle: "tmp song title",
        textCreatorUser: new User(
          name: creator.name,
          email: creator.email,
          imageUrl: creator.imageUrl,
          uid: creator.uid,
        ));
  }
}
